document.addEventListener("DOMContentLoaded", function() {
  function Inflation(percent, clean_profit) {
    return parseInt(clean_profit / 100 * (100 - percent), 10)
  };

document.querySelector(".form__button").addEventListener("click", function() {
   var expenses_regular = 0;
   var category = all_read_category();
   var category_is_valid = true;
   category.forEach(function(item, i, arr) {
	if (item.Name == '' || item.Count == '')
	    category_is_valid = false;
	else
	    expenses_regular += parseInt(item.Count, 10);
   });
   console.log(category);

   if (category_is_valid == false || category.length == 0) {
	alert("Укажите категории!");
	return;
   }

   var input = {
       CountFamily: parseInt(document.querySelector("input[name='CountFamily']").value, 10),
       ProfitRegular: parseInt(document.querySelector("input[name='RegularProfit']").value, 10),
       ProfitNotRegular: parseInt(document.querySelector("input[name='NotRegularProfit']").value, 10),
       ExpensesRegular: parseInt(expenses_regular, 10),
       ExpensesNotRegular: parseInt(document.querySelector("input[name='NotRegularExpenses']").value, 10),
       Target: parseInt(document.querySelector("input[name='Target']").value, 10)
   };

   var ProfitBestWay = Inflation(20, input.ProfitRegular + input.ProfitNotRegular);
   var ExpensesBestWay = input.ExpensesRegular;
   var CleanProfitBestWay = Inflation(20, input.ProfitRegular + input.ProfitNotRegular - input.ExpensesRegular);

   var ProfitWorstWay = Inflation(20, input.ProfitRegular);
   var ExpensesWorstWay = input.ExpensesRegular + input.ExpensesNotRegular;
   var CleanProfitWorstWay = Inflation(20, input.ProfitRegular - input.ExpensesRegular - input.ExpensesNotRegular);

   var output = {
     BestWay: {
       Profit: ProfitBestWay,
       Expenses: ExpensesBestWay,
       CleanProfit: CleanProfitBestWay
     },
     WorstWay: {
       Profit: ProfitWorstWay,
       Expenses: ExpensesWorstWay,
       CleanProfit: CleanProfitWorstWay
     },
     AverageWay: {
        Profit: (ProfitBestWay + ProfitWorstWay) / 2,
        Expenses: (ExpensesBestWay + ExpensesWorstWay) / 2,
        CleanProfit: (CleanProfitBestWay + CleanProfitWorstWay) / 2
     },
     Categories: all_read_category().sort(function(a, b){
        return b.Count - a.Count;
    }),
      CountMonthsToTarget: parseInt(input.Target / ((CleanProfitBestWay + CleanProfitWorstWay) / 2), 10) + 1, // Если = 0, то мы никогда не сможем покорить эту цель
      CountFamily: input.CountFamily
    }

    if (isNaN(output.BestWay.CleanProfit) || isNaN(output.WorstWay.CleanProfit))
      alert("Ошибка! Укажите правильные значения!");
    else {
      console.log(input);
      console.log(output);

      // Если запускать локально (как файл html, то браузер игнорировает cookies)
      //setCookie('ForIndex', JSON.stringify(output));
      document.cookie = "for_index=" + JSON.stringify(output) + ";"; 
      self.location = "index.html";
    }
  });

  document.querySelector(".add-category").addEventListener("click", function () {
    let div = document.createElement("div");
    let name = document.createElement("input");
    let count = document.createElement("input");

    div.setAttribute("class","category-item");
    div.setAttribute("id", "category-item" + get_count_in_category());

    name.setAttribute("placeholder", "Название");
    name.setAttribute("class", "category-name");
    name.setAttribute("type", "text");

    count.setAttribute("placeholder", "Количество");
    count.setAttribute("class", "category-count");
    count.setAttribute("type", "number");
    count.setAttribute("min", "0");
    count.setAttribute("oninput", "validity.valid||(value='');");

    document.querySelector(".category-container").append(div);
    div.append(name);
    div.append(count);
    console.log(all_read_category());
  })

  document.querySelector(".remove-category").addEventListener("click", function () {
    let div = document.querySelector(".category-item:last-child");
    document.querySelector(".category-item:last-child").remove(div);

  })
})

function get_count_in_category() {
    var i = document.querySelectorAll(".category-item").length + 1;
    // console.log('get count in category: ' + i)
    return i;
}

function all_read_category() {
    if (get_count_in_category() == 0)
	alert("Выбирайте категорию!");

    var i = 1;
    var elements = [];
    while(true) {
	var element = document.getElementById("category-item" + i)
	console.log("element" + i + " = " + element)

      if (element == null)
	  return elements;

	elements.push({
	  Name: element.getElementsByClassName("category-name")[0].value,
	  Count: element.getElementsByClassName("category-count")[0].value
	});

	i++;
  }
}
