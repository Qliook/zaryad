var chart3 = c3.generate({
    bindto: '#chart3',
    data: {
      columns:
      [
        ['Лучший вариант', 30, 200, 100, 400, 150, 250, 30, 200, 100, 400, 150, 250],
	      ['Оптимальный вариант', 20, 190, 90, 350, 140, 230, 20, 100, 120, 300, 130, 240],
        ['Худший вариант', 50, 20, 10, 40, 15, 25]
      ],
      types:
      {
        data1: "bar",
        data2: "bar"
      }
    },
    axis: {
        x: {
            type: 'category',
            categories: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            tick: {
                // count: 50,
            }
        },
        y: {
            show: true,
            tick: {
                count: 6
            }
        }
    }
});

const swiper = new Swiper('.swiper', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,

    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  });



function get_cookie_for_index() {
  var results = document.cookie.match(/for_index=(.+?)(;|$)/);
  console.log(results);
  return results[1]; // user
}

// decodeURIComponent(document.cookie)
console.log("Cookie: " + get_cookie_for_index());
output = JSON.parse(get_cookie_for_index());
console.log(output);
render_result(output.AverageWay.Profit, output.AverageWay.Expenses);

let ol = document.querySelector(".ol")
output.Categories.forEach(element => {
  let li = document.createElement("li")
  ol.append(li)
  li.textContent = element.Name + ': ' + element.Count
  console.log("Foreach: " + element)
});

function render_result(profit, expenses) {
  c3.generate({
    // bindto: "#char3",
    data: {
        // iris data from R
        columns: [
            ['Доходы', profit],
            ['Расходы', expenses],
        ],
        type : 'pie',
        //onclick: function (d, i) { console.log("onclick", d, i); },
        //onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        //onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    }
  });
  
  var chart2 = c3.generate({
    bindto: '#chart2',
    data: {
      columns:
      [
        ['Доходы', 30, 200, 100, profit , 150, 250, 30, 200, 100, 400, 150, 250],
        ['Расходы', 50, 20, 10, expenses , 15, 25]
      ],
    },
    axis: {
        x: {
            type: 'category',
            categories: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            tick: {
                // count: 50,
            }
        },
        y: {
            show: true,
            tick: {
                count: 6
            }
        }
    }
});
};


document.querySelector('#burger').addEventListener('click', function () {
    document.querySelector('#menu').classList.add('menu__active')
})

document.querySelector('.close').addEventListener('click', function () {
  document.querySelector('#menu').classList.remove('menu__active')
})

var for_c3_expenses = [];
output.Categories.forEach(element => for_c3_expenses.push([element.Name, element.Count]));
console.log('for_c3_expenses: ' + for_c3_expenses);

c3.generate({
  data: {
      columns: for_c3_expenses,
      type : 'pie'
    }
});

